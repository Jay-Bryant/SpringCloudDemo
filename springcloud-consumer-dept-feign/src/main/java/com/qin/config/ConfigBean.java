package com.qin.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * FileName: ConfigBean
 * Author: qinSX
 * Data: 2022/06/26 -- 19:28
 * Description:
 */
@Configuration
public class ConfigBean {

    // 负载均衡实现RestTemplate
    //IRule
    @Bean
    @LoadBalanced  // ribbon
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

}
