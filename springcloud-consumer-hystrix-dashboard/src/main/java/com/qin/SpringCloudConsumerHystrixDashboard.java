package com.qin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * FileName: SpringCloudConsimerHystrixDashboard
 * Author: qinSX
 * Data: 2022/06/27 -- 21:38
 * Description:
 */
@SpringBootApplication
@EnableHystrixDashboard // 开机监控
public class SpringCloudConsumerHystrixDashboard {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudConsumerHystrixDashboard.class, args);
    }
}
