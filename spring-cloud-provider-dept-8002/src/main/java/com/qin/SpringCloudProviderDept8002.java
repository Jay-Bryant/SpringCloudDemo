package com.qin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * FileName: SpringCloudProviderDept8002
 * Author: qinSX
 * Data: 2022/06/27 -- 11:13
 * Description:
 */
@SpringBootApplication
@MapperScan("com.qin.dao")
@EnableEurekaClient
public class SpringCloudProviderDept8002 {
    public static void main(String[] args) {
        SpringApplication.run(SpringCloudProviderDept8002.class, args);
    }
}
