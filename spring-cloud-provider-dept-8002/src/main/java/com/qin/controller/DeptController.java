package com.qin.controller;

import com.qin.entity.Dept;
import com.qin.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * FileName: DeptController
 * Author: qinSX
 * Data: 2022/06/26 -- 16:32
 * Description:
 */
@RestController
public class DeptController {

    @Autowired
    private DeptService deptService;

    @PostMapping("/dept/add")
    boolean addDept(Dept dept) {
        System.out.println("负载均衡----------------PROVIDER--8002--------");
        return deptService.addDept(dept);
    }

    @GetMapping("/dept/get/{id}")
    Dept getDept(@PathVariable("id") long id) {
        System.out.println("负载均衡----------------PROVIDER--8002--------");
        return deptService.queryById(id);
    }

    @GetMapping("/dept/list")
    List<Dept> queryAll() {
        System.out.println("负载均衡----------------PROVIDER--8002--------");
        return deptService.queryAll();
    }
}
