# SpringCloudDemo

#### 介绍
SpringCloud记录

学习SpringCloud一定要注意控制依赖版本

#### 软件架构
>   
    1.服务降级：不管在什么情况下，服务降级的流程都是先调用正常的方法，再调用fallback的方法。 
    也就是服务器繁忙，请稍后再试，不让客户端等待并立刻返回一个友好提示。
    
    2.服务熔断：假设服务宕机或者在单位时间内调用服务失败的次数过多，
    即服务降级的次数太多，那么则服务熔断。 
    并且熔断以后会跳过正常的方法，会直接调用fallback方法，
    即所谓“服务熔断后不可用”。
    类似于家里常见的保险丝，当达到最大服务访问后，会直接拒绝访问，拉闸限电，然后调用服务降级的fallback方法，返回友好提示。
    spring-boot-dependencies -- 2.7.0
    spring-cloud-dependencies -- 2021.0.0
    eureka：
        spring-cloud-starter-netflix-eureka-server -- 3.1.2
        spring-cloud-starter-netflix-eureka-client -- 3.1.2
    feign:(接口编程)
        spring-cloud-starter-feign -- 1.4.6.RELEASE
    hystrix:(服务熔断与降级)
        spring-cloud-starter-netflix-hystrix -- 2.2.9.RELEASE
    ribbon:(负载均衡)
        spring-cloud-starter-netflix-ribbon 禁止导入依赖
        spring-cloud-starter-loadbalancer 在spring cloud新版 有此负载均衡依赖（防止包冲突 启动失败）
    集群模式：
        Eureka集群 -- 2
        服务端 -- 3 （包含一个熔断机制）
    注意：
        启动成功很费内存,注意电脑内存,和启动顺序







