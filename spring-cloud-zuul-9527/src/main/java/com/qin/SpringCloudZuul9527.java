package com.qin;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * FileName: SpringCloudZuul9527
 * Author: qinSX
 * Data: 2022/06/28 -- 10:36
 * Description:
 */
@SpringBootApplication
@EnableZuulProxy // 开启代理
public class SpringCloudZuul9527 {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudZuul9527.class, args);
    }
}
