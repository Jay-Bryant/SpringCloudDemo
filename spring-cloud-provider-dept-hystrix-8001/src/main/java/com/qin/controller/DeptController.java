package com.qin.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.qin.entity.Dept;
import com.qin.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * FileName: DeptController
 * Author: qinSX
 * Data: 2022/06/26 -- 16:32
 * Description:
 */
@RestController
public class DeptController {

    @Autowired
    private DeptService deptService;

    @PostMapping("/dept/add")
    boolean addDept(Dept dept) {
        System.out.println("负载均衡----------------PROVIDER--8001--------");
        return deptService.addDept(dept);
    }

    @HystrixCommand(fallbackMethod = "hystrixGet")
    @GetMapping("/dept/get/{id}")
    Dept getDept(@PathVariable("id") long id) {
        System.out.println("负载均衡------Hystrix----------------PROVIDER--8001--------");

        Dept dept = deptService.queryById(id);
        if (dept == null) {
            throw new RuntimeException("id=>" + id + "不存在该用户，或者信息无法找到");
        }
        return dept;

    }

    // 备选方案
    public Dept hystrixGet(@PathVariable("id") long id) {
        Dept dept = new Dept()
                .setDeptNo(id)
                .setDName("id=>" + id + "不存在该用户,null@Hystrix")
                .setDSource("NO THIS DATABASE IN MYSQL");
        return dept;
    }

    @GetMapping("/dept/list")
    List<Dept> queryAll() {
        System.out.println("负载均衡----------------PROVIDER--8001--------");
        return deptService.queryAll();
    }
}
