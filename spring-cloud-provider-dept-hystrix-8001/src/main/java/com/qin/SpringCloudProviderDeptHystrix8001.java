package com.qin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

/**
 * FileName: SpringCloudProvderDeptHystrix8001
 * Author: qinSX
 * Data: 2022/06/27 -- 14:47
 * Description:
 */
@SpringBootApplication
@MapperScan("com.qin.dao")
@EnableEurekaClient
@EnableHystrix  // 熔断注解支持
public class SpringCloudProviderDeptHystrix8001 {
    public static void main(String[] args) {
        SpringApplication.run(SpringCloudProviderDeptHystrix8001.class, args);
    }
}
