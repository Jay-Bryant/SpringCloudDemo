package com.qin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * FileName: SpringCloudEurekaApplication
 * Author: qinSX
 * Data: 2022/06/26 -- 21:27
 * Description:
 */
@SpringBootApplication
@EnableEurekaServer
public class SpringCloudEureka7001 {
    public static void main(String[] args) {
        SpringApplication.run(SpringCloudEureka7001.class, args);
    }
}
