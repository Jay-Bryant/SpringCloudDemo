package com.qin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * FileName: SpringCloudConsumerApplication
 * Author: qinSX
 * Data: 2022/06/26 -- 19:18
 * Description:
 */
@SpringBootApplication
@EnableEurekaClient
public class SpringCloudConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudConsumerApplication.class, args);
    }
}
