package com.qin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * FileName: springcloudEureka7002
 * Author: qinSX
 * Data: 2022/06/27 -- 10:32
 * Description:
 */
@SpringBootApplication
@EnableEurekaServer
public class SpringCloudEureka7002 {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudEureka7002.class, args);
    }
}
