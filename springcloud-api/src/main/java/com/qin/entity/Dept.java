package com.qin.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * FileName: Dept
 * Author: qinSX
 * Data: 2022/06/26 -- 13:46
 * Description:
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)  // 链式写法
public class Dept implements Serializable {

    private Long deptNo;
    private String dName;
    private String dSource;

    public Dept(String dName) {
        this.dName = dName;
    }
}
