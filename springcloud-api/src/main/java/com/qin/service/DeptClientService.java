package com.qin.service;

import com.qin.entity.Dept;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

/**
 * FileName: DeptClientService
 * Author: qinSX
 * Data: 2022/06/27 -- 14:09
 * Description:
 */
@Service
@FeignClient(value = "SPRING-CLOUD-PROVIDER",fallbackFactory = DeptClientServiceFallbackFactory.class)   // 代表调用哪个微服务 面向接口编程
public interface DeptClientService {

    @GetMapping("/dept/get/{id}")
    Dept queryById(@PathVariable("id") long id);

    @GetMapping("/dept/list")
    List<Dept> queryAll();

    @PostMapping("/dept/add")
    boolean addDept(Dept dept);

}
