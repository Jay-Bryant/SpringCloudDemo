package com.qin.service;

import com.qin.entity.Dept;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * FileName: DeptClientServiceFallbackFactory
 * Author: qinSX
 * Data: 2022/06/27 -- 21:13
 * Description:
 */

/**
 * 降级 ~
* */
@Component
public class DeptClientServiceFallbackFactory implements FallbackFactory {
    @Override
    public DeptClientService create(Throwable cause) {
        return new DeptClientService() {
            @Override
            public Dept queryById(long id) {
                return new Dept().setDeptNo(id)
                        .setDName("ID->"+id+"没有对应的信息，客户端提供降级处理，这个服务现在已经关闭")
                        .setDSource("没有数据");
            }

            @Override
            public List<Dept> queryAll() {
                return null;
            }

            @Override
            public boolean addDept(Dept dept) {
                return false;
            }
        };
    }
}
