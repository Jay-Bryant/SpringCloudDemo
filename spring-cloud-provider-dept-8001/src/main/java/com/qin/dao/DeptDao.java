package com.qin.dao;

import com.qin.entity.Dept;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * FileName: DeptDao
 * Author: qinSX
 * Data: 2022/06/26 -- 14:41
 * Description:
 */

@Repository
public interface DeptDao {

    @Results(value = {
            @Result(column = "d_name",property = "dName"),
            @Result(column = "d_source",property = "dSource")
    })
    @Insert("insert db_dept(d_name, d_source) value (#{dName}, #{dSource})")
    boolean addDept(Dept dept);

    @Results(value = {
            @Result(column = "dept_no",property = "deptNo"),
            @Result(column = "d_name",property = "dName"),
            @Result(column = "d_source",property = "dSource")
    })
    @Select("select * from db_dept where dept_no = #{id}")
    Dept queryById(Long id);

    @Results(value = {
            @Result(column = "dept_no",property = "deptNo"),
            @Result(column = "d_name",property = "dName"),
            @Result(column = "d_source",property = "dSource")
    })
    @Select("select * from db_dept")
    List<Dept> queryAll();

}
