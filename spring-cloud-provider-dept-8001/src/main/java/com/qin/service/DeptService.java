package com.qin.service;

import com.qin.entity.Dept;

import java.util.List;

/**
 * FileName: DeptService
 * Author: qinSX
 * Data: 2022/06/26 -- 15:42
 * Description:
 */
public interface DeptService {

    boolean addDept(Dept dept);

    Dept queryById(Long id);

    List<Dept> queryAll();

}
