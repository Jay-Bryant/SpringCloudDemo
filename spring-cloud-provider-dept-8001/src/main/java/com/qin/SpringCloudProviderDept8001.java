package com.qin;

import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

/**
 * FileName: SpringCloudProviderDept8001
 * Author: qinSX
 * Data: 2022/06/26 -- 14:11
 * Description:
 */
@SpringBootApplication
@MapperScan("com.qin.dao")
@EnableEurekaClient
public class SpringCloudProviderDept8001 {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudProviderDept8001.class, args);
    }

    // 添加servlet
    @Bean
    public ServletRegistrationBean servletRegistrationBean() {

        ServletRegistrationBean registrationBean = new ServletRegistrationBean(new HystrixMetricsStreamServlet());
        registrationBean.addUrlMappings("/actuator/hystrix.stream");
        return registrationBean;
    }
}
